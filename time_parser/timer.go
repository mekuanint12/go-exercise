package timetester

import (
	"strconv"
	"time"
)

func MonthTimer(newTime time.Time) string {
	_, month, _ := newTime.Date()
	h, m, s := newTime.Clock()
	parsedTime := strconv.Itoa(h) + ":" + strconv.Itoa(m) + ":" + strconv.Itoa(s) + " - " + month.String()
	return parsedTime
}

// func main() {
// 	theTime := time.Now()
// 	ParsedTime := MonthTimer(theTime)
// 	fmt.Println(ParsedTime)

// }
