package timetester

import (
	"testing"
	"time"
)

func TestTimeParser(t *testing.T) {
	testTimer, _ := time.Parse("2006-01-02 03:04:05", "2022-01-01 02:30:45")
	t.Run("Check the parser", func(t *testing.T) {
		got := MonthTimer(testTimer)
		want := "2:30:45 - January"
		if got != want {
			t.Errorf("got : %v but want : %v", got, want)
		}

	})
}
